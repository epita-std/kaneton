/*
 * ---------- header ----------------------------------------------------------
 *
 * project       kaneton
 *
 * license       kaneton
 *
 * file          /home/mycure/kane...ine/glue/ibm-pc.ia32/educational/event.c
 *
 * created       renaud voltz   [mon feb 13 01:05:52 2006]
 * updated       julien quintard   [sat feb  5 12:30:16 2011]
 */

/*
 * ---------- information -----------------------------------------------------
 *
 * this file implements the event manager's glue.
 */

/*
 * ---------- includes --------------------------------------------------------
 */

#include <kaneton.h>
#include "include/idt_extern.h"

/*
 * ---------- globals ---------------------------------------------------------
 */

/*
 * the event dispatcher.
 */

d_event			glue_event_dispatch =
  {
    NULL,
    NULL,
    NULL,
    glue_event_enable,
    glue_event_disable,
    glue_event_reserve,
    glue_event_release,
    glue_event_initialize,
    NULL
  };

/* FIXED at K1 */

/*
 * ---------- functions -------------------------------------------------------
 */

t_error			glue_event_enable(void)
{
  ARCHITECTURE_STI();

  MACHINE_LEAVE();
}

t_error			glue_event_disable(void)
{
  ARCHITECTURE_CLI();

  MACHINE_LEAVE();
}

t_error			glue_event_reserve(i_event id,
					   t_type type,
					   u_event_handler h,
					   t_data data)
{
  if (id >= ARCHITECTURE_IDT_IRQ_BASE &&
    id < ARCHITECTURE_IDT_IRQ_SIZE + ARCHITECTURE_IDT_IRQ_BASE)
    platform_pic_enable(id - ARCHITECTURE_IDT_IRQ_BASE);


  MACHINE_LEAVE();
}

t_error			glue_event_release(i_event id)
{
  if (id >= ARCHITECTURE_IDT_IRQ_BASE &&
    id < ARCHITECTURE_IDT_IRQ_SIZE + ARCHITECTURE_IDT_IRQ_BASE)
    platform_pic_disable(id - ARCHITECTURE_IDT_IRQ_BASE);

  MACHINE_LEAVE();
}

t_error			glue_event_initialize(void)
{
  t_uint16 i;

  if (architecture_idt_build() != ERROR_OK)
    MACHINE_ESCAPE("unable to initialize the IDT");

  architecture_idt_insert(0, (t_paddr) &idt_wrapper_0, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(1, (t_paddr) &idt_wrapper_1, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(2, (t_paddr) &idt_wrapper_2, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(3, (t_paddr) &idt_wrapper_3, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(4, (t_paddr) &idt_wrapper_4, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(5, (t_paddr) &idt_wrapper_5, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(6, (t_paddr) &idt_wrapper_6, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(7, (t_paddr) &idt_wrapper_7, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(8, (t_paddr) &idt_wrapper_8, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(9, (t_paddr) &idt_wrapper_9, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(10, (t_paddr) &idt_wrapper_10, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(11, (t_paddr) &idt_wrapper_11, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(12, (t_paddr) &idt_wrapper_12, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(13, (t_paddr) &idt_wrapper_13, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(14, (t_paddr) &idt_wrapper_14, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(15, (t_paddr) &idt_wrapper_15, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(16, (t_paddr) &idt_wrapper_16, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(17, (t_paddr) &idt_wrapper_17, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(18, (t_paddr) &idt_wrapper_18, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(19, (t_paddr) &idt_wrapper_19, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(20, (t_paddr) &idt_wrapper_20, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(21, (t_paddr) &idt_wrapper_21, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(22, (t_paddr) &idt_wrapper_22, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(23, (t_paddr) &idt_wrapper_23, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(24, (t_paddr) &idt_wrapper_24, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(25, (t_paddr) &idt_wrapper_25, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(26, (t_paddr) &idt_wrapper_26, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(27, (t_paddr) &idt_wrapper_27, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(28, (t_paddr) &idt_wrapper_28, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(29, (t_paddr) &idt_wrapper_29, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(30, (t_paddr) &idt_wrapper_30, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(31, (t_paddr) &idt_wrapper_31, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(32, (t_paddr) &idt_wrapper_32, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(33, (t_paddr) &idt_wrapper_33, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(34, (t_paddr) &idt_wrapper_34, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(35, (t_paddr) &idt_wrapper_35, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(36, (t_paddr) &idt_wrapper_36, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(37, (t_paddr) &idt_wrapper_37, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(38, (t_paddr) &idt_wrapper_38, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(39, (t_paddr) &idt_wrapper_39, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(40, (t_paddr) &idt_wrapper_40, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(41, (t_paddr) &idt_wrapper_41, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(42, (t_paddr) &idt_wrapper_42, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(43, (t_paddr) &idt_wrapper_43, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(44, (t_paddr) &idt_wrapper_44, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(45, (t_paddr) &idt_wrapper_45, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(46, (t_paddr) &idt_wrapper_46, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(47, (t_paddr) &idt_wrapper_47, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(48, (t_paddr) &idt_wrapper_48, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(49, (t_paddr) &idt_wrapper_49, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(50, (t_paddr) &idt_wrapper_50, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(51, (t_paddr) &idt_wrapper_51, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(52, (t_paddr) &idt_wrapper_52, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(53, (t_paddr) &idt_wrapper_53, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(54, (t_paddr) &idt_wrapper_54, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(55, (t_paddr) &idt_wrapper_55, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(56, (t_paddr) &idt_wrapper_56, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(57, (t_paddr) &idt_wrapper_57, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(58, (t_paddr) &idt_wrapper_58, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(59, (t_paddr) &idt_wrapper_59, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(60, (t_paddr) &idt_wrapper_60, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(61, (t_paddr) &idt_wrapper_61, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(62, (t_paddr) &idt_wrapper_62, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(63, (t_paddr) &idt_wrapper_63, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(64, (t_paddr) &idt_wrapper_64, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(65, (t_paddr) &idt_wrapper_65, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(66, (t_paddr) &idt_wrapper_66, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(67, (t_paddr) &idt_wrapper_67, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(68, (t_paddr) &idt_wrapper_68, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(69, (t_paddr) &idt_wrapper_69, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(70, (t_paddr) &idt_wrapper_70, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(71, (t_paddr) &idt_wrapper_71, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(72, (t_paddr) &idt_wrapper_72, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(73, (t_paddr) &idt_wrapper_73, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(74, (t_paddr) &idt_wrapper_74, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(75, (t_paddr) &idt_wrapper_75, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(76, (t_paddr) &idt_wrapper_76, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(77, (t_paddr) &idt_wrapper_77, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(78, (t_paddr) &idt_wrapper_78, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(79, (t_paddr) &idt_wrapper_79, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(80, (t_paddr) &idt_wrapper_80, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(81, (t_paddr) &idt_wrapper_81, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(82, (t_paddr) &idt_wrapper_82, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(83, (t_paddr) &idt_wrapper_83, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(84, (t_paddr) &idt_wrapper_84, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(85, (t_paddr) &idt_wrapper_85, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(86, (t_paddr) &idt_wrapper_86, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(87, (t_paddr) &idt_wrapper_87, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(88, (t_paddr) &idt_wrapper_88, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(89, (t_paddr) &idt_wrapper_89, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(90, (t_paddr) &idt_wrapper_90, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(91, (t_paddr) &idt_wrapper_91, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(92, (t_paddr) &idt_wrapper_92, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(93, (t_paddr) &idt_wrapper_93, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(94, (t_paddr) &idt_wrapper_94, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(95, (t_paddr) &idt_wrapper_95, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(96, (t_paddr) &idt_wrapper_96, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(97, (t_paddr) &idt_wrapper_97, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(98, (t_paddr) &idt_wrapper_98, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(99, (t_paddr) &idt_wrapper_99, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(100, (t_paddr) &idt_wrapper_100, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(101, (t_paddr) &idt_wrapper_101, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(102, (t_paddr) &idt_wrapper_102, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(103, (t_paddr) &idt_wrapper_103, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(104, (t_paddr) &idt_wrapper_104, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(105, (t_paddr) &idt_wrapper_105, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(106, (t_paddr) &idt_wrapper_106, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(107, (t_paddr) &idt_wrapper_107, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(108, (t_paddr) &idt_wrapper_108, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(109, (t_paddr) &idt_wrapper_109, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(110, (t_paddr) &idt_wrapper_110, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(111, (t_paddr) &idt_wrapper_111, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(112, (t_paddr) &idt_wrapper_112, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(113, (t_paddr) &idt_wrapper_113, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(114, (t_paddr) &idt_wrapper_114, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(115, (t_paddr) &idt_wrapper_115, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(116, (t_paddr) &idt_wrapper_116, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(117, (t_paddr) &idt_wrapper_117, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(118, (t_paddr) &idt_wrapper_118, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(119, (t_paddr) &idt_wrapper_119, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(120, (t_paddr) &idt_wrapper_120, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(121, (t_paddr) &idt_wrapper_121, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(122, (t_paddr) &idt_wrapper_122, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(123, (t_paddr) &idt_wrapper_123, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(124, (t_paddr) &idt_wrapper_124, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(125, (t_paddr) &idt_wrapper_125, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(126, (t_paddr) &idt_wrapper_126, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(127, (t_paddr) &idt_wrapper_127, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(128, (t_paddr) &idt_wrapper_128, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(129, (t_paddr) &idt_wrapper_129, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(130, (t_paddr) &idt_wrapper_130, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(131, (t_paddr) &idt_wrapper_131, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(132, (t_paddr) &idt_wrapper_132, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(133, (t_paddr) &idt_wrapper_133, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(134, (t_paddr) &idt_wrapper_134, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(135, (t_paddr) &idt_wrapper_135, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(136, (t_paddr) &idt_wrapper_136, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(137, (t_paddr) &idt_wrapper_137, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(138, (t_paddr) &idt_wrapper_138, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(139, (t_paddr) &idt_wrapper_139, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(140, (t_paddr) &idt_wrapper_140, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(141, (t_paddr) &idt_wrapper_141, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(142, (t_paddr) &idt_wrapper_142, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(143, (t_paddr) &idt_wrapper_143, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(144, (t_paddr) &idt_wrapper_144, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(145, (t_paddr) &idt_wrapper_145, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(146, (t_paddr) &idt_wrapper_146, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(147, (t_paddr) &idt_wrapper_147, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(148, (t_paddr) &idt_wrapper_148, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(149, (t_paddr) &idt_wrapper_149, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(150, (t_paddr) &idt_wrapper_150, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(151, (t_paddr) &idt_wrapper_151, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(152, (t_paddr) &idt_wrapper_152, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(153, (t_paddr) &idt_wrapper_153, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(154, (t_paddr) &idt_wrapper_154, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(155, (t_paddr) &idt_wrapper_155, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(156, (t_paddr) &idt_wrapper_156, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(157, (t_paddr) &idt_wrapper_157, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(158, (t_paddr) &idt_wrapper_158, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(159, (t_paddr) &idt_wrapper_159, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(160, (t_paddr) &idt_wrapper_160, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(161, (t_paddr) &idt_wrapper_161, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(162, (t_paddr) &idt_wrapper_162, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(163, (t_paddr) &idt_wrapper_163, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(164, (t_paddr) &idt_wrapper_164, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(165, (t_paddr) &idt_wrapper_165, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(166, (t_paddr) &idt_wrapper_166, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(167, (t_paddr) &idt_wrapper_167, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(168, (t_paddr) &idt_wrapper_168, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(169, (t_paddr) &idt_wrapper_169, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(170, (t_paddr) &idt_wrapper_170, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(171, (t_paddr) &idt_wrapper_171, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(172, (t_paddr) &idt_wrapper_172, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(173, (t_paddr) &idt_wrapper_173, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(174, (t_paddr) &idt_wrapper_174, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(175, (t_paddr) &idt_wrapper_175, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(176, (t_paddr) &idt_wrapper_176, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(177, (t_paddr) &idt_wrapper_177, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(178, (t_paddr) &idt_wrapper_178, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(179, (t_paddr) &idt_wrapper_179, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(180, (t_paddr) &idt_wrapper_180, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(181, (t_paddr) &idt_wrapper_181, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(182, (t_paddr) &idt_wrapper_182, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(183, (t_paddr) &idt_wrapper_183, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(184, (t_paddr) &idt_wrapper_184, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(185, (t_paddr) &idt_wrapper_185, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(186, (t_paddr) &idt_wrapper_186, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(187, (t_paddr) &idt_wrapper_187, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(188, (t_paddr) &idt_wrapper_188, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(189, (t_paddr) &idt_wrapper_189, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(190, (t_paddr) &idt_wrapper_190, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(191, (t_paddr) &idt_wrapper_191, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(192, (t_paddr) &idt_wrapper_192, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(193, (t_paddr) &idt_wrapper_193, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(194, (t_paddr) &idt_wrapper_194, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(195, (t_paddr) &idt_wrapper_195, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(196, (t_paddr) &idt_wrapper_196, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(197, (t_paddr) &idt_wrapper_197, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(198, (t_paddr) &idt_wrapper_198, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(199, (t_paddr) &idt_wrapper_199, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(200, (t_paddr) &idt_wrapper_200, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(201, (t_paddr) &idt_wrapper_201, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(202, (t_paddr) &idt_wrapper_202, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(203, (t_paddr) &idt_wrapper_203, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(204, (t_paddr) &idt_wrapper_204, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(205, (t_paddr) &idt_wrapper_205, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(206, (t_paddr) &idt_wrapper_206, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(207, (t_paddr) &idt_wrapper_207, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(208, (t_paddr) &idt_wrapper_208, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(209, (t_paddr) &idt_wrapper_209, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(210, (t_paddr) &idt_wrapper_210, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(211, (t_paddr) &idt_wrapper_211, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(212, (t_paddr) &idt_wrapper_212, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(213, (t_paddr) &idt_wrapper_213, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(214, (t_paddr) &idt_wrapper_214, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(215, (t_paddr) &idt_wrapper_215, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(216, (t_paddr) &idt_wrapper_216, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(217, (t_paddr) &idt_wrapper_217, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(218, (t_paddr) &idt_wrapper_218, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(219, (t_paddr) &idt_wrapper_219, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(220, (t_paddr) &idt_wrapper_220, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(221, (t_paddr) &idt_wrapper_221, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(222, (t_paddr) &idt_wrapper_222, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(223, (t_paddr) &idt_wrapper_223, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(224, (t_paddr) &idt_wrapper_224, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(225, (t_paddr) &idt_wrapper_225, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(226, (t_paddr) &idt_wrapper_226, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(227, (t_paddr) &idt_wrapper_227, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(228, (t_paddr) &idt_wrapper_228, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(229, (t_paddr) &idt_wrapper_229, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(230, (t_paddr) &idt_wrapper_230, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(231, (t_paddr) &idt_wrapper_231, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(232, (t_paddr) &idt_wrapper_232, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(233, (t_paddr) &idt_wrapper_233, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(234, (t_paddr) &idt_wrapper_234, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(235, (t_paddr) &idt_wrapper_235, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(236, (t_paddr) &idt_wrapper_236, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(237, (t_paddr) &idt_wrapper_237, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(238, (t_paddr) &idt_wrapper_238, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(239, (t_paddr) &idt_wrapper_239, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(240, (t_paddr) &idt_wrapper_240, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(241, (t_paddr) &idt_wrapper_241, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(242, (t_paddr) &idt_wrapper_242, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(243, (t_paddr) &idt_wrapper_243, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(244, (t_paddr) &idt_wrapper_244, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(245, (t_paddr) &idt_wrapper_245, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(246, (t_paddr) &idt_wrapper_246, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(247, (t_paddr) &idt_wrapper_247, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(248, (t_paddr) &idt_wrapper_248, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(249, (t_paddr) &idt_wrapper_249, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(250, (t_paddr) &idt_wrapper_250, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(251, (t_paddr) &idt_wrapper_251, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(252, (t_paddr) &idt_wrapper_252, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(253, (t_paddr) &idt_wrapper_253, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(254, (t_paddr) &idt_wrapper_254, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));
  architecture_idt_insert(255, (t_paddr) &idt_wrapper_255, ARCHITECTURE_IDTE_DPL_SET(ARCHITECTURE_PRIVILEGE_RING0));

  if (architecture_idt_import() != ERROR_OK)
    MACHINE_ESCAPE("unable to load the IDT");

  if (architecture_idt_enable() != ERROR_OK)
    MACHINE_ESCAPE("unable to load the IDT");

  //t_uint16 test = 0;
  //test = 50 / test;

  MACHINE_LEAVE();
}

/* EOFIX */
