#!/bin/sh

if [ -n "$1" ]
then
    FILEOUT=$1
else
    FILEOUT=${0:0:$(($#0 - 3))}
fi

echo ".section .text" > $FILEOUT
echo "        .align  4" >> $FILEOUT
echo "" >> $FILEOUT

for i in `seq 0 255`
do
    echo "        .global idt_wrapper_$i" >> $FILEOUT
    echo "idt_wrapper_$i:" >> $FILEOUT
    echo "        cli" >> $FILEOUT
    echo "        pusha" >> $FILEOUT
    echo "        push %ds" >> $FILEOUT
    echo "        push %es" >> $FILEOUT
    echo "        push %fs" >> $FILEOUT
    echo "        push %gs" >> $FILEOUT
    echo "        push %ss" >> $FILEOUT

    echo "        mov \$0x10, %ax" >> $FILEOUT
    echo "        mov %ax, %ds" >> $FILEOUT
    echo "        mov %ax, %es" >> $FILEOUT
    echo "        mov %ax, %fs" >> $FILEOUT
    echo "        mov %ax, %gs" >> $FILEOUT
    echo "        mov %ax, %ss" >> $FILEOUT

    echo "        push \$$i" >> $FILEOUT
    echo "        call handler" >> $FILEOUT
    echo "        pop %ebx" >> $FILEOUT
    echo "        pop %ss" >> $FILEOUT
    echo "        pop %gs" >> $FILEOUT
    echo "        pop %fs" >> $FILEOUT
    echo "        pop %es" >> $FILEOUT
    echo "        pop %ds" >> $FILEOUT
    echo "        popa" >> $FILEOUT
    if [ $i -eq 8 ] || [ $i -eq 17 ] || ( [ $i -ge 10 ] && [ $i -lt 15 ])
    then
	echo "        add \$4, %esp" >> $FILEOUT
    fi
    echo "        sti" >> $FILEOUT
    echo "        iret" >> $FILEOUT
    echo "" >> $FILEOUT
done
