/*
 * ---------- header ----------------------------------------------------------
 *
 * project       kaneton
 *
 * license       kaneton
 *
 * file          /home/mycure/kane...hitecture/ia32/educational/include/idt.h
 *
 * created       renaud voltz   [fri feb 10 16:36:20 2006]
 * updated       julien quintard   [mon apr 11 13:45:20 2011]
 */

#ifndef ARCHITECTURE_IDT_H
#define ARCHITECTURE_IDT_H	1

/*
 * ---------- macros ----------------------------------------------------------
 */

/*
 * these macros define the base entry and the number of entries for the
 * several types of gate: IRQ, exception, IPI or syscall.
 *
 * note that 200 syscalls could be set up but the kernel limits itself
 * to ten which is enough for a microkernel.
 */

#define ARCHITECTURE_IDT_EXCEPTION_BASE		0
#define ARCHITECTURE_IDT_EXCEPTION_SIZE		32

#define ARCHITECTURE_IDT_IRQ_BASE		32
#define ARCHITECTURE_IDT_IRQ_SIZE		16

#define ARCHITECTURE_IDT_IPI_BASE		48
#define ARCHITECTURE_IDT_IPI_SIZE		8

#define ARCHITECTURE_IDT_SYSCALL_BASE		56
#define ARCHITECTURE_IDT_SYSCALL_SIZE		10

/*
 * these macro define some of the exception handler sources.
 */

#define ARCHITECTURE_IDT_EXCEPTION_DE					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 0
#define ARCHITECTURE_IDT_EXCEPTION_DB					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 1
#define ARCHITECTURE_IDT_EXCEPTION_BP					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 3
#define ARCHITECTURE_IDT_EXCEPTION_OF					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 4
#define ARCHITECTURE_IDT_EXCEPTION_BR					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 5
#define ARCHITECTURE_IDT_EXCEPTION_UD					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 6
#define ARCHITECTURE_IDT_EXCEPTION_NM					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 7
#define ARCHITECTURE_IDT_EXCEPTION_DF					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 8
#define ARCHITECTURE_IDT_EXCEPTION_TS					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 10
#define ARCHITECTURE_IDT_EXCEPTION_NP					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 11
#define ARCHITECTURE_IDT_EXCEPTION_SS					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 12
#define ARCHITECTURE_IDT_EXCEPTION_GP					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 13
#define ARCHITECTURE_IDT_EXCEPTION_PF					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 14
#define ARCHITECTURE_IDT_EXCEPTION_MF					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 16
#define ARCHITECTURE_IDT_EXCEPTION_AC					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 17
#define ARCHITECTURE_IDT_EXCEPTION_MC					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 18
#define ARCHITECTURE_IDT_EXCEPTION_XM					\
  ARCHITECTURE_IDT_EXCEPTION_BASE + 19

/*
 * these macro define some of the IRQ handler sources.
 */

#define ARCHITECTURE_IDT_IRQ_PIT					\
  ARCHITECTURE_IDT_IRQ_BASE + 0
#define ARCHITECTURE_IDT_IRQ_KEYBOARD					\
  ARCHITECTURE_IDT_IRQ_BASE + 1
#define ARCHITECTURE_IDT_IRQ_CASCADE					\
  ARCHITECTURE_IDT_IRQ_BASE + 2
#define ARCHITECTURE_IDT_IRQ_COM2					\
  ARCHITECTURE_IDT_IRQ_BASE + 3
#define ARCHITECTURE_IDT_IRQ_COM1					\
  ARCHITECTURE_IDT_IRQ_BASE + 4
#define ARCHITECTURE_IDT_IRQ_FLOPPY					\
  ARCHITECTURE_IDT_IRQ_BASE + 6
#define ARCHITECTURE_IDT_IRQ_SPURIOUS					\
  ARCHITECTURE_IDT_IRQ_BASE + 7
#define ARCHITECTURE_IDT_IRQ_RTC					\
  ARCHITECTURE_IDT_IRQ_BASE + 8
#define ARCHITECTURE_IDT_IRQ_ATA1					\
  ARCHITECTURE_IDT_IRQ_BASE + 14
#define ARCHITECTURE_IDT_IRQ_ATA2					\
  ARCHITECTURE_IDT_IRQ_BASE + 15


/* Fixed at K1 */
#define ARCHITECTURE_IDT_SIZE           256


#define ARCHITECTURE_IDTE_TASK          (5LL << 40)
#define ARCHITECTURE_IDTE_INTERRUPT     (6LL << 40)
#define ARCHITECTURE_IDTE_TRAP          (7LL << 40)

#define ARCHITECTURE_IDTE_32BIT			(1LL << 43)
#define ARCHITECTURE_IDTE_PRESENT		(1LL << 47)

#define ARCHITECTURE_IDTE_OFFSET_SET(_offset_)				\
  (at_idte)((((at_idte)(_offset_) & 0x0000ffff)) |			\
	    (((at_idte)(_offset_) & 0xffff0000) << 32))

#define ARCHITECTURE_IDTE_OFFSET_GET(_idte_)				\
  (t_paddr)((((_idte_) >> 0) & 0x0000ffff) |				\
	    (((_idte_) >> 32) & 0xffff0000))

#define ARCHITECTURE_IDTE_DPL_SET(_privilege_)				\
  (((at_idte)(_privilege_) & 0x3) << 45)

#define ARCHITECTURE_IDTE_DPL_GET(_idte_)				\
  (((_idte_) >> 45) & 0x3)

#define ARCHITECTURE_IDTE_SEGMENT_SET(_segment_)			\
  ((((t_uint32) _segment_) & 0xffff) << 16)

/* EOFIXME */

/*
 * ---------- dependencies ----------------------------------------------------
 */

#include <core/types.h>

/*
 * ---------- types -----------------------------------------------------------
 */

typedef t_uint64	at_idte;

typedef struct
{
  t_uint16		limit;
  t_paddr		base;
}			__attribute__ ((packed)) as_idtr;

/*
 * ---------- prototypes ------------------------------------------------------
 *
 *      ../idt.c
 */

/*
 * ../idt.c
 */

t_error	architecture_idt_dump(void);

t_error			architecture_idt_import(void);

t_error			architecture_idt_enable(void);

t_error			architecture_idt_disable(void);

t_error	architecture_idt_insert(t_uint16	index,
				t_paddr		offset,
				t_flags		flags);

t_error	architecture_idt_reserve(t_paddr	base,
				 t_flags	flags,
				 t_uint16*	index);

t_error	architecture_idt_delete(t_uint16	index);

t_error	architecture_idt_build(void);


/*
 * eop
 */

#endif
