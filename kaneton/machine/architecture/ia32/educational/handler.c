/*
 * ---------- header ----------------------------------------------------------
 *
 * project       kaneton
 *
 * license       kaneton
 *
 * file          /home/mycure/kane.../architecture/ia32/educational/handler.c
 *
 * created       renaud voltz   [thu feb 23 10:49:43 2006]
 * updated       julien quintard   [mon apr 11 13:44:48 2011]
 */

/*
 * ---------- includes --------------------------------------------------------
 */

#include <kaneton.h>

/*
 * ---------- functions -------------------------------------------------------
 */

/* FIXME[complete if necessary] */
void handler(int num)
{
  o_event *irs;

  if (event_exist(num) == ERROR_TRUE)
  {
    event_get(num, &irs);
    irs->handler.routine(irs->id, irs->data);
  }

  if (num >= ARCHITECTURE_IDT_IRQ_BASE &&
      num < ARCHITECTURE_IDT_IRQ_SIZE + ARCHITECTURE_IDT_IRQ_BASE)
    platform_pic_acknowledge(num - ARCHITECTURE_IDT_IRQ_BASE);
}
